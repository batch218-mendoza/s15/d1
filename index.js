console.log("hello world!");
// enclose with quotation mark string datas
console. log("hello world")
// case sensitive


console. 
log
( 
	"hello, everyone!"
)

// ; delimeter
// we use delimeter to end our code

// [comments]
// -single line comments

/* multi line comment*/
// ctrl + shift + /
// ctrl + /



// Syntax and statement
// statements in programming are instructions that we tell to computer to perform
// syntax in programming, it is the set of rules that describes how statements must be considered


// Variables


let myVariable = "hello";
console.log("myVariable");

let temperature = 82;
console.log("temperature");

// console.log(hello); will result to not defined error


/*
	Guides in writing variables
		1. Use the 'let' keyword followed by the variable name of your choosing  and use the assignment operator(=) to assign a value
		2. Variable names should start with a lowercase character, use camelCase for multiple words.
		3. for constant variables. use the 'const' keyword
		4. variable names should be indicative or descriptive of the value being stored.
		5. never name a variable starting with numbers.
		6. Refrain from using space in declaring a variable
*/

// String let can be reassigned
let productName = 'desktop computer';
console.log(productName);

let product = "Alvin's computer";
console.log(product);

// Number
let productPrice = 18999;
console.log(productPrice);

// cannot be reassigned const
const interest = 3.539;
console.log(interest);

// Reassigning variable value
// Syntax
	// variableName =  NewValue;


productName = "Laptop";
console.log(productName);

let friend = "kate";
friend = "Jane";
console.log(friend);

const pi = 3.14

let supplier; // declaration
supplier = "John Smith Trading" // initializing
console.log(supplier);

supplier = "Zuitt Store";
console.log(supplier);

// Multiple Variable declaration
let productCode = "DC017";
const productBrand = "Dell";
console.log(productCode,productBrand);

// Using a variable with a reserved keyword
// const let = "hello";
// console.log(let);

// [SECTION] Data Types

// Strings
// Strings are series of characters that create a word, phrase, sentence, or anything related to creating text.
// Strings in Javascript is enclosed with single ('') or double ("")qoute

let country = 'Philippines';
let province = "Metro Manila";
console.log(province + ', ' + country);
// we use + symbol to concatenate data / values

let fullAddress = province + ', ' + country;
console.log(fullAddress);

console.log("Philippines" + ', ' + "Metro Manila");

// Escape Character (\)
// "\n" refers to creating a new line or set the text to next line;


console.log("line1\nline2");

let mailAddress = "metro manila\nPhilippines";
console.log(mailAddress);

let message = "John's employee went home early."
console.log(message);

message = 'John\'s employee went home early.'
console.log(message);

// Numbers


// integers
let headcount = 26;
console.log(headcount);

// Decimal numbers / float
let grade = 98.7;
console.log(grade);

// Exponential notation

let planetDistance = 2e10;
console.log(planetDistance);

console.log(2.3 + grade);

console.log("jhon's grade last sem is" + grade)

// Arrays

//it is store multiple values with similar data type
let grades = [ 98.7, 95.4, 90.2];
console.log(grades);


// storing different data types inside an array is not recommended because it will not make sense in the context of programming.
let details = ["john", "smith", 32, true];
//arrayname / elements
console.log(details)

//objects
//objects are another special kind of data type that used to mimic real world objects/items

// syntax:

/*
 	let/construct objectname = {
		propertyA: value,
		propertyB: value
 	}
*/
// key / property / value
let person = {
	fullName: "Juan Dela Cruz",
	age: 35,
	isMarried: false,
	contact: ["0912 345 6789", "8123 7444"],
	/*address: {
		houseNumber: "345",
		city: "manila"
	}*/
};

console.log(person);


const myGrades = {
	firstGrading: 98.7,
	secondGrading: 95.4,
	thirdGrading: 90.2,
	fourthGrading: 94.6
};

console.log(myGrades);

let stringValue = "string";
let numberValue = 10;
let booleanValue = true;
let waterBills = [1000, 640, 700];
// myGrades as object

// 'Typeof' -
// we use type of operator to retrieve / know the data type

console.log(typeof stringValue); //output: string
console.log(typeof numberValue); //output: number
console.log(typeof booleanValue); //output: boolean
console.log(typeof waterBills); //output: array
console.log(typeof myGrades); //output: object

// Constant Objects and Arrays
// We cannot reassign the value of the variable, But we can change the elements of the constant array

const anime = ["One Piece", "Code Geas", "Monster", "Dan Maci", "Demon Slayer"];
// index - is the position of the element starting zero.

anime[0] = "Naruto";
console.log(anime);

// Null
// it is used to intentionally express the absence of a value

let spouse = null;
console.log(spouse);

// undefined
// represents the state of a variable that has been declared but without an assigned value

let fullname;
console.log(fullname);